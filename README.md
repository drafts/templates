# Documents building templates

These templates are to ease the process to build papers or presentations.

## tl;dr

Simply use the following code as your `.gitlab-ci.yml` and name your paper `paper.tex` and your supplementary material (if available) `supplementary.tex`.  This setup will build your paper without extra effort.

```yaml
include:
  - project: 'drafts/templates'
    file:
      - '.latexmk.yml'
```

# LaTeX Make (latexmk)

The standard build assumes that the paper and the supplementary material are

```yaml
variables:
  FILE: "paper"
  SUP: "supplementary"
```
Note that the **names of the files contain no extensions**.  The build process automatically appends the proper extension to find the `.pdf` or the `.tex` files.

If you want to override these variables, you can do so by redefining them after importing the template.  For example

```yaml
include:
  - project: 'drafts/templates'
    file: '.latexmk.yml'

variables:
  FILE: "my-awesome-paper"
```

will include the template, and build `my-awesome-paper.tex`.  You can do the same for the `SUP` variable if needed.

## LaTeX Enginge

The default LaTeX engine is `pdflatex`.  In case you want to run some other engine using the `pdflatex` setup from `latexmk` you can redefine the variable `PROGRAM` within the CI.  For instance, you will do
```yaml
variables:
  PROGRAM: "lualatex"
```
after including the template.


# Reply Letter CI

The reply letter template will build on a reply letter that lives within `REPLY_DIR` directory and that is named `REPLY`.  Also, it will do a `diff` of the main paper in a `TAG` and the latest version.

The default variable names are
```yaml
variables:
  REPLY_DIR: "repply-letter"
  REPLY: "reply-letter"
  APPENDIX: "appendix"
  TAG: "review"
```

When you use the reply you also need to include the paper build CI.  So, the minimal `.gitlab-ci.yml` is

```yaml
include:
  - project: 'drafts/templates'
    file:
      - '.latexmk.yml'
      - '.reply-ci.yml'
```

## Appendix

Note that the build assumes that you have an `$APPENDIX.tex` file.  This setup assumes that your supplementary material includes this appendix file within it, and your main paper can also use it.  This split simplifies building papers that include the additional material within them, like for arXiv, or with supplementary material outside it, like for conferences or journals.

The template searches for the existence of such file and then builds the files accordingly.  If you use it with another name, you can always re-define it through the variables.
